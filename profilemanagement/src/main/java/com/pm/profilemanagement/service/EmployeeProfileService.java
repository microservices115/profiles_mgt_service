package com.pm.profilemanagement.service;

import com.pm.profilemanagement.entity.EmployeeProfile;

import java.util.List;

public interface EmployeeProfileService {
    void addEmployeeProfile(EmployeeProfile profile);
    List < EmployeeProfile > getEmployeeProfiles();
}