package com.pm.profilemanagement.dao;

import com.pm.profilemanagement.entity.EmployeeProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends JpaRepository <EmployeeProfile, Integer > {

}